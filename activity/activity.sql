--> [1] Adding records to the "users" table
INSERT INTO users (email, password, datetime_created) 
VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00"),
("johnsmith@gmail.com", "passwordB", "2021-01-01 02:00:00"),
("juandelacruz@gmail.com", "passwordC", "2021-01-01 03:00:00"),
("janesmith@gmail.com", "passwordD", "2021-01-01 04:00:00"),
("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");


--> [2] Adding records to the "posts" table
INSERT INTO posts (user_id, title, content, datetime_posted) 
VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00"),
(1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00"),
(2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00"),
(4, "Fourth Code", "Bye bye Solar System!", "2021-01-02 04:00:00");


--> [3] Getting all the post with an Author ID of 1
SELECT * FROM posts WHERE user_id = 1;


--> [4] Getting all the user's email and datetime of creation
SELECT email, datetime_created FROM users;


--> [5] Updating a post's content
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!" AND user_id = 1;


--> [6] Deleting the user with an email of "johndoe@gmail.com"
DELETE FROM users WHERE email = "johndoe@gmail.com";